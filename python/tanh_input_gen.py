import numpy as np
import sys

# int quantize(float *input, unsigned char *output, unsigned long size, float *scale, unsigned char *offset)
# {
#     for (unsigned long i = 0; i < size; ++i) {
#         float quantized = input[i] / scale[0] + offset[0];
#         if(quantized > 255) {
#             output[i] = 255;
#         }
#         else if(quantized < 0) {
#             output[i] = 0;
#         }
#         else {
#             output[i] = (unsigned char) ones_round(quantized);
#         }
#     }
#     return 0;
# }

# int dequantize(const unsigned char *input, float *output, unsigned long size,
#             float *scale, unsigned char *zero_point, int channel_size, bool use_stride)
# {
#     if (channel_size < 1) {
#         return -1;
#     }
#     for (unsigned long i = 0; i < size; ++i) {
#         int channel_index;
#         if (use_stride) {
#             int stride = size / channel_size;
#             channel_index = (i / stride) % channel_size;
#         } else {
#             if (channel_size > 1) {
#                 channel_index = i % channel_size;
#             } else {
#                 channel_index = 0;
#             }
#         }
#         output[i] = (input[i] - zero_point[channel_index]) * scale[channel_index];
#     }
#     return 0;
# }
def dequantize(input, scale, zero_point):
    output=np.zeros(len(input)).astype(np.float32)
    for i in range(len(input)):
        output[i] = ((input[i] - zero_point) * scale).astype(np.float32)
    return output

def quantize(input, scale, zero_point):
    output=np.zeros(len(input)).astype(np.uint8)
    for i in range(len(input)):
        quantized = input[i] / scale + zero_point
        if(quantized > 255):
            output[i] = 255
        elif(quantized < 0):
            output[i] = 0
        else:
            output[i] = np.round(quantized)
    return output

def create_random_8bit_int_array(length):
    # 주어진 길이만큼의 8-bit 정수 배열을 random한 값으로 생성
    return np.random.randint(0, 255, size=length, dtype=np.uint8)

def load_8bit_int_array_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8)

def load_32bit_float_array_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.float32)

def save_array_to_file(filename, data, is_float=False):
    if is_float:
        # Float 데이터를 float 형식의 이진 파일로 저장
        data.astype(np.float32).tofile(filename)
    else:
        # uint8 데이터를 이진 파일로 저장
        data.tofile(filename)

def apply_tanh_to_array(array):
    # 주어진 배열에 tanh 함수를 적용
    # print("array",array)
    tanh_applied = np.tanh(array)
    # print("tanh_applied",tanh_applied)
   
    return tanh_applied



def test_input_values(input_array):
    # 입력 배열의 값 범위 확인
    min_value = np.min(input_array)
    max_value = np.max(input_array)
    assert min_value >= 0 and max_value <= 255, f"Input array values out of range: min={min_value}, max={max_value}"
    

def test_tanh_application(tanh_result):
    # tanh 함수 적용 결과 확인
    assert np.min(tanh_result) >= -1 and np.max(tanh_result) <= 1, f"Tanh result out of range: min={np.min(tanh_result)}, max={np.max(tanh_result)}"

def test_file_io_int(filename, expected_array):
    # 파일 저장 및 읽기 테스트
    save_array_to_file(filename, expected_array)
    loaded_array = load_8bit_int_array_from_file(filename)
    # print("loaded_array",loaded_array)
    # print("expected_array",expected_array)
    assert np.array_equal(expected_array, loaded_array), "Saved and loaded arrays do not match"
    
def test_file_io_float(filename, expected_array):
    # 파일 저장 및 읽기 테스트
    save_array_to_file(filename, expected_array,True)
    loaded_array = load_32bit_float_array_from_file(filename)
    # print("loaded_array",loaded_array)
    # print("expected_array",expected_array)
    assert np.allclose(expected_array, loaded_array,atol=0.00001), "Saved and loaded arrays do not match"

def main():
    if len(sys.argv) != 3:
        length = 100
    else:
        length = int(sys.argv[1])
    # 균일 분포를 갖는 입력 배열 생성
    input_array = create_random_8bit_int_array(length)
    # print(f"Input array ({length} elements):", input_array)
    
    # 테스트: 입력 배열 값 범위 확인
    test_input_values(input_array)
    # print("size of input_array",input_array.size)
    test_file_io_int("input.bin", input_array)
    
    output_array1 = dequantize(input_array, 0.01, 128.0)
    # print(f"Output array ({length} elements):", output_array1)

    # tanh 함수 적용 및 결과 스케일링
    tanh_result = apply_tanh_to_array(output_array1)    
    tanh_result = np.floor(tanh_result * 1e6) / 1e6

    # 테스트: tanh 함수 적용 결과 확인
    test_tanh_application(tanh_result)
    # print("Scaled result array:", tanh_result)
    
    output_array2 = quantize(tanh_result, 0.01, 128.0)
    # print(f"Output array ({length} elements):", output_array2)


    # 결과를 파일에 저장하고 파일 입출력 테스트
    filename = "golden.bin"
    test_file_io_int(filename, output_array2)
    # print(f"Result saved to {filename} and file I/O test passed")

if __name__ == "__main__":
    main()
