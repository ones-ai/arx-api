import numpy as np
import torch
import torch.nn.functional as F

# set options
MAX_INPUT_ELEMENT = 30
MAX_INPUT_DIM_LENGTH = 4
MAX_KERNEL_DIM_LENGTH = 5
MAX_BIAS_ELEMENT = 5
SHIFT = 5
STRIDE_SIZE = 1
PADDING_SIZE = 1
OUTPUT_OFFSET = 1

def create_random_8bit_input_tensor(max_dimension_length, max_element_size):
    # (N, C, H, W) 형태의 uint8 입력 텐서를 생성합니다.
    N = int(np.random.randint(3, max_dimension_length, dtype=np.uint8))
    H = int(np.random.randint(3, max_dimension_length, dtype=np.uint8))
    W = int(np.random.randint(3, max_dimension_length, dtype=np.uint8))
    C = int(np.random.randint(3, max_dimension_length, dtype=np.uint8))
    return N, H, W, C, torch.randint(0, 255, (N, C, H, W), dtype=torch.uint8)

def create_random_uint8_kernel_tensor(N, H, W, C):
    # (KN, C, KH, KW) 형태의 int8 커널 텐서를 생성합니다.
    # 여기서는 -127 이상 127 이하의 값을 생성합니다.
    KN = int(np.random.randint(1, N - 1, dtype=np.uint8))
    KH = int(np.random.randint(1, H - 1, dtype=np.uint8))
    KW = int(np.random.randint(1, W - 1, dtype=np.uint8))
    kernel_tensor = torch.randint(0, 255, (KN, C, KH, KW), dtype=torch.uint8)
    return KN, KH, KW, C, kernel_tensor

def save_8bit_int_array_to_file(filename, data):
    data.tofile(filename)

def load_8bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

def save_32bit_int_array_to_file(filename, data):
    data.tofile(filename)

def main():
    # 1. 텐서 생성
    N, H, W, C, input_tensor = create_random_8bit_input_tensor(MAX_INPUT_DIM_LENGTH, MAX_INPUT_ELEMENT)
    KN, KH, KW, C, kernel_tensor = create_random_uint8_kernel_tensor(N, H, W, C)
    bias_tensor = torch.randint(0, MAX_BIAS_ELEMENT + 1, size=(KN,), dtype=torch.int32)
    out_H = 1 + (H + 2 * PADDING_SIZE - KH) // STRIDE_SIZE
    out_W = 1 + (W + 2 * PADDING_SIZE - KW) // STRIDE_SIZE

    arguments = np.array([N, H, W, C, KN, KH, KW, PADDING_SIZE, STRIDE_SIZE, SHIFT, out_H, out_W], dtype=np.uint8)
    #                     0  1  2  3  4   5    6        7           8          9      10     11   
    # 2. 파일 저장
    # 입력 tensor: (N, C, H, W) → NHWC 순서, flatten 후 저장
    save_8bit_int_array_to_file("input.bin", input_tensor.permute(0, 2, 3, 1).reshape(-1).numpy())
    
    # [수정된 부분]
    # kernel_tensor는 int8 범위(-127 ~ 127)를 가지므로,
    # 저장 전에 각 원소에 127을 더해 uint8로 변환합니다.
    kernel_int32 = ((kernel_tensor.to(torch.int32) - 127).to(torch.int32))
    kernel_int32 = torch.clamp(kernel_int32,-127,128).to(torch.int32)
    
    # kernel tensor를 NHWC 순서 (KN, KH, KW, C)로 변경 후 flatten하여 저장
    save_8bit_int_array_to_file("kernel_int32.bin", kernel_int32.permute(0, 2, 3, 1).reshape(-1).numpy())
    save_8bit_int_array_to_file("kernel.bin", kernel_tensor.permute(0, 2, 3, 1).reshape(-1).numpy())
    save_32bit_int_array_to_file("bias.bin", bias_tensor.numpy())
    save_8bit_int_array_to_file("arguments.bin", arguments)
   
    
    # 3. 파일로부터 로드 및 텐서 복원
    input_tensor = torch.tensor(load_8bit_int_array_from_file("input.bin"), dtype=torch.uint8)\
                           .view(N, H, W, C).permute(0, 3, 1, 2)
    kernel_tensor = torch.tensor(load_8bit_int_array_from_file("kernel.bin"), dtype=torch.uint8)\
                           .view(KN, KH, KW, C).permute(0, 3, 1, 2)
    
    # 4. convolution 연산 (F.conv2d 사용)
    output_tensor_before_shift = F.conv2d(input_tensor.to(torch.int32),
                                          kernel_int32.to(torch.int32),
                                          bias=bias_tensor.to(torch.int32),
                                          stride=STRIDE_SIZE,
                                          padding=PADDING_SIZE)
    
    # 수정된 코드:
    rounding = 1 << (SHIFT - 1)
    # 먼저 output_tensor_before_shift의 음수를 0으로 처리 (underflow 해결)
    output_tensor_before_shift = torch.clamp(output_tensor_before_shift, min=0)
    # rounding을 더하고 시프트 연산 수행
    output_tensor = (output_tensor_before_shift + rounding) >> SHIFT
    # OUTPUT_OFFSET을 더함
    output_tensor = output_tensor + OUTPUT_OFFSET
    # 최종적으로 255를 초과하는 값을 255로 clamp (overflow 해결)
    output_tensor = torch.clamp(output_tensor, max=255).to(torch.uint8)
    
    save_8bit_int_array_to_file("golden.bin", output_tensor.permute(0, 2, 3, 1).reshape(-1).numpy())
    
   

    #print("\nc_output_tensor",c_output_tensor)
if __name__ == "__main__":
    main()
