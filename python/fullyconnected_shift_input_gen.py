import numpy as np
import torch
import torch.nn.functional as F

# set options
MAX_INPUT_ELEMENT = 15
MAX_INPUT_DIM_LENGTH = 4
MAX_WEIGHT_ELEMENT = 3
MAX_OUTPUT_DIM_LENGTH = 4
MAX_BIAS_ELEMENT = 3   # bias값은 0~3 범위 (원하는 범위 0~MAX_BIAS_ELEMENT)
MAX_SHIFT = 3
DO_BIAS = True
SHIFT = 9
OUTPUT_OFFSET = 1

def create_random_8bit_input_tensor(max_dimension_length, max_element_size):
    # (N, C, H, W) 형태의 uint8 입력 텐서를 생성합니다.
    in_dim0 = int(np.random.randint(3, max_dimension_length, dtype=np.uint8))
    in_dim1 = int(np.random.randint(3, max_dimension_length, dtype=np.uint8))
    return in_dim0, in_dim1, torch.randint(0, 255, (in_dim0, in_dim1), dtype=torch.uint8)

def create_random_8bit_weight_tensor(max_element_size, in_dim0, in_dim1, out_dim0, out_dim1):
    # weight tensor (fully-connected): shape (kernel_dim0, kernel_dim1)
    # kernel_dim0 = out_dim0 * out_dim1, kernel_dim1 = in_dim0 * in_dim1
    kernel_dim0 = out_dim0 * out_dim1
    kernel_dim1 = in_dim0 * in_dim1
    return kernel_dim0, kernel_dim1, torch.randint(0, 255, (kernel_dim0, kernel_dim1), dtype=torch.uint8)

def save_8bit_int_array_to_file(filename, data):
    data.tofile(filename)

def load_8bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

def save_32bit_int_array_to_file(filename, data):
    data.tofile(filename)

def main():
    # 1. 입력 텐서 생성
    in_dim0, in_dim1, input_tensor = create_random_8bit_input_tensor(MAX_INPUT_DIM_LENGTH, MAX_INPUT_ELEMENT)
    
    # 2. 출력 차원 생성 (예: out_dim0와 out_dim1)
    out_dim0 = int(np.random.randint(3, MAX_OUTPUT_DIM_LENGTH, dtype=np.uint8))
    out_dim1 = int(np.random.randint(3, MAX_OUTPUT_DIM_LENGTH, dtype=np.uint8))
    
    # 3. weight tensor 생성
    kernel_dim0, kernel_dim1, kernel_tensor = create_random_8bit_weight_tensor(MAX_WEIGHT_ELEMENT, in_dim0, in_dim1, out_dim0, out_dim1)
    
    # 4. bias tensor 생성: bias의 크기는 kernel_dim0 (즉, out_dim0*out_dim1)와 일치해야 합니다.
    bias_tensor = torch.randint(0, MAX_BIAS_ELEMENT + 1, size=(kernel_dim0,), dtype=torch.int32)
    
    # Fully-connected operation에서는
    # input_tensor (in_dim0 x in_dim1) reshaped to (in_dim0*in_dim1,)
    # weight: kernel_tensor of shape (kernel_dim0, kernel_dim1)
    # 그 결과는 (kernel_dim0,) 크기의 벡터가 됩니다.
    
    # 5. arguments 배열 생성: [in_dim0, in_dim1, kernel_dim0, kernel_dim1, DO_BIAS, SHIFT, out_dim0, out_dim1]
    arguments = np.array([in_dim0, in_dim1, kernel_dim0, kernel_dim1, DO_BIAS, SHIFT, out_dim0, out_dim1], dtype=np.uint8)
    
    # 6. 파일로 저장 (flatten 후)
    save_8bit_int_array_to_file("input.bin", input_tensor.reshape(-1).numpy())
    save_8bit_int_array_to_file("kernel.bin", kernel_tensor.reshape(-1).numpy())
    save_32bit_int_array_to_file("bias.bin", bias_tensor.numpy())
    save_8bit_int_array_to_file("arguments.bin", arguments)
    
    # 7. 여기서 kernel_tensor는 uint8 값(0~255)이며,
    #    실제로 사용해야 할 kernel은 int8 값로, 즉 (kernel_tensor - 127)로 계산해야 합니다.
    kernel_int32 = kernel_tensor.to(torch.int32) - 127
    kernel_int32 = torch.clamp(kernel_int32, -127, 128).to(torch.int32)
    
    # 8. fully connected 연산
    # Reshape input_tensor to (in_dim0*in_dim1,)
    output_tensor = input_tensor.to(torch.int32).reshape(-1) @ kernel_int32.to(torch.int32)
    # 출력 벡터에 bias를 더합니다. (두 벡터의 크기는 kernel_dim0)
    output_tensor = output_tensor + bias_tensor
    
    # 9. SHIFT 연산
    rounding = 1 << (SHIFT - 1)
    # 먼저 output_tensor_before_shift의 음수를 0으로 처리 (underflow 해결)
   
    # rounding을 더하고 시프트 연산 수행
    output_tensor = (output_tensor + rounding) >> SHIFT
    # OUTPUT_OFFSET을 더함
    output_tensor = output_tensor + OUTPUT_OFFSET
    # 최종적으로 255를 초과하는 값을 255로 clamp (overflow 해결)
    output_tensor = torch.clamp(output_tensor, min=0)
    output_tensor = torch.clamp(output_tensor, max=255).to(torch.uint8)
    
    save_8bit_int_array_to_file("golden.bin", output_tensor.reshape(-1).numpy())
    
    print("Input Tensor:")
    print(input_tensor)
    print("\nKernel Tensor (uint8):")
    print(kernel_tensor)
    print("\nKernel Int32 (kernel_tensor - 127):")
    print(kernel_int32)
    print("\nBias Tensor (int32):")
    print(bias_tensor)
    print("\nOutput Tensor:")
    print(output_tensor)
    
if __name__ == "__main__":
    main()
