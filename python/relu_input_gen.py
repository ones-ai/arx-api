import numpy as np
import sys

def create_random_8bit_int_array(length):
    # 주어진 길이만큼의 8-bit 정수 배열을 random한 값으로 생성
    return np.random.randint(0, 255, size=length, dtype=np.uint8)

def load_8bit_int_array_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8)

def save_8bit_int_array_to_file(filename, data):
    # 파일에 8-bit 정수 배열을 저장
    data.tofile(filename)

def relu_arrays(array1):
    # 배열 ReLU 연산
    result = np.maximum(0, array1).astype(np.uint8)
    return result

def main():
    if len(sys.argv) != 2:
        length = 100
    else:
        length = int(sys.argv[1])

    # 주어진 길이만큼의 random한 8-bit integer array 생성
    array1 = create_random_8bit_int_array(length)

    # 배열을 input1.bin을 파일에 저장
    save_8bit_int_array_to_file("input1.bin", array1)

    # 저장된 배열을 다시 불러옴
    array1 = load_8bit_int_array_from_file("input1.bin")

    # 배열 소프트맥스 연산
    result = relu_arrays(array1)

    # 결과를 출력 파일에 저장
    save_8bit_int_array_to_file("golden.bin", result)

if __name__ == "__main__":
    main()
