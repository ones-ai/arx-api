import argparse
import sys

def read_file(filename):
    with open(filename, "rb") as f:
        return list(f.read())

def compare_files(file1_data, file2_data):
    if len(file1_data) != len(file2_data):
        print("The two files have different lengths.")
        sys.exit(1)

    differences = 0
    diff_positions = []

    for i, (byte1, byte2) in enumerate(zip(file1_data, file2_data)):
        if byte1 != byte2:
            differences += 1
            diff_positions.append(i)

    if differences:
        print(f"Total number of differing values between the two files: {differences}")
        print("Positions and values of differing values:")
        for pos in diff_positions:
            print(f"Position {pos}: Value in File 1: {file1_data[pos]}, Value in File 2: {file2_data[pos]}")
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(description="Compare two binary files.")
    parser.add_argument("file1", type=str, help="Name of the first file")
    parser.add_argument("file2", type=str, help="Name of the second file")

    args = parser.parse_args()

    file1_data = read_file(args.file1)
    file2_data = read_file(args.file2)

    compare_files(file1_data, file2_data)

if __name__ == "__main__":
    main()
