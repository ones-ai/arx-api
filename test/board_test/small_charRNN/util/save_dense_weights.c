#include <stdio.h>
#include <stdlib.h>

#define RNN_UNITS 64         // 예시 값, 실제 값에 맞게 수정 필요
#define VOCAB_SIZE 65       // 예시 값, 실제 값에 맞게 수정 필요

// 배열 크기 정의
#define WOUT_SIZE (RNN_UNITS * VOCAB_SIZE)
#define BOUT_SIZE VOCAB_SIZE

// Function to extract weights from Wout and bout arrays and save to a header file
void extract_and_save_dense_weights(const float *Wout, const float *bout, const char *header_file) {
    FILE *header_fp = fopen(header_file, "w");
    if (!header_fp) {
        perror("Failed to open header file");
        exit(EXIT_FAILURE);
    }

    // Declare weight and bias arrays
    float Dense_Wout[RNN_UNITS][VOCAB_SIZE];
    float Dense_bout[VOCAB_SIZE];

    // Extract weights from Wout array
    for (int i = 0; i < RNN_UNITS; ++i) {
        for (int j = 0; j < VOCAB_SIZE; ++j) {
            Dense_Wout[i][j] = Wout[i * VOCAB_SIZE + j];
        }
    }

    // Extract biases from bout array
    for (int i = 0; i < VOCAB_SIZE; ++i) {
        Dense_bout[i] = bout[i];
    }

    // Write header file
    fprintf(header_fp, "#ifndef DENSE_WEIGHTS_H\n");
    fprintf(header_fp, "#define DENSE_WEIGHTS_H\n\n");

    fprintf(header_fp, "#define RNN_UNITS %d\n", RNN_UNITS);
    fprintf(header_fp, "#define VOCAB_SIZE %d\n", VOCAB_SIZE);

    fprintf(header_fp, "float Dense_Wout[RNN_UNITS][VOCAB_SIZE] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < VOCAB_SIZE; ++j) {
            fprintf(header_fp, "%.6f", Dense_Wout[i][j]);
            if (j < VOCAB_SIZE - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float Dense_bout[VOCAB_SIZE] = {\n");
    for (int i = 0; i < VOCAB_SIZE; ++i) {
        fprintf(header_fp, "    %.6f", Dense_bout[i]);
        if (i < VOCAB_SIZE - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "#endif // DENSE_WEIGHTS_H\n");

    // Close file
    fclose(header_fp);
    printf("Successfully generated header file: %s\n", header_file);
}

int main() {
    const char *Wout_file = "param/dense_weight_0.bin";
    const char *bout_file = "param/dense_weight_1.bin";
    const char *header_file = "build/include/small_dense_weights.h";
    // Read Wout and bout data from binary files
    FILE *bin_fp = fopen(Wout_file, "rb");
    if (!bin_fp) {
        perror("Failed to open binary file");
        exit(EXIT_FAILURE);
    }

    float *Wout = (float *)malloc(WOUT_SIZE * sizeof(float));
    if (!Wout) {
        perror("Failed to allocate memory");
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }

    if (fread(Wout, sizeof(float), WOUT_SIZE, bin_fp) != WOUT_SIZE) {
        perror("Error reading binary file");
        free(Wout);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }
    fclose(bin_fp);

    bin_fp = fopen(bout_file, "rb");
    if (!bin_fp) {
        perror("Failed to open binary file");
        free(Wout);
        exit(EXIT_FAILURE);
    }

    float *bout = (float *)malloc(BOUT_SIZE * sizeof(float));
    if (!bout) {
        perror("Failed to allocate memory");
        free(Wout);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }

    if (fread(bout, sizeof(float), BOUT_SIZE, bin_fp) != BOUT_SIZE) {
        perror("Error reading binary file");
        free(Wout);
        free(bout);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }
    fclose(bin_fp);

    // Extract weights and save to header file
    extract_and_save_dense_weights(Wout, bout, header_file);

    // Free allocated memory
    free(Wout);
    free(bout);

    return 0;
}
