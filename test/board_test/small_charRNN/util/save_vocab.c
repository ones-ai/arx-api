#include <stdio.h>
#include <stdlib.h>

void bin_to_header(const char *bin_file, const char *header_file, const char *array_name, size_t size) {
    FILE *bin_fp = fopen(bin_file, "rb");
    if (!bin_fp) {
        perror("Failed to open binary file");
        exit(EXIT_FAILURE);
    }

    FILE *header_fp = fopen(header_file, "w");
    if (!header_fp) {
        perror("Failed to open header file");
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }

    // 헤더 파일의 시작 부분에 include 가드와 배열 선언 작성
    fprintf(header_fp, "#ifndef %s_H\n", array_name);
    fprintf(header_fp, "#define %s_H\n\n", array_name);
    fprintf(header_fp, "#define VOCAB_SIZE %zu\n", size);
    fprintf(header_fp, "char %s[VOCAB_SIZE] = {\n", array_name);

    // 이진 파일에서 데이터를 읽어서 배열에 저장
    char value;
    for (size_t i = 0; i < size; ++i) {
        if (fread(&value, sizeof(char), 1, bin_fp) != 1) {
            perror("Error reading binary file");
            fclose(bin_fp);
            fclose(header_fp);
            exit(EXIT_FAILURE);
        }
        fprintf(header_fp, "%d", value); // 각 문자를 정수로 출력
        if (i < size - 1) {
            fprintf(header_fp, ", ");
        }
    }

    // 배열과 include 가드 마무리
    fprintf(header_fp, "\n};\n\n");
    fprintf(header_fp, "#endif // %s_H\n", array_name);

    // 파일 닫기
    fclose(bin_fp);
    fclose(header_fp);
    printf("Successfully generated header file: %s\n", header_file);
}

int main() {
    const char *vocab_file = "param/idx2char.bin"; // 사용자가 읽을 파일 경로를 설정합니다.
    const char *vocab_header_file = "build/include/vocab.h";
    
    size_t vocab_size = 65; // 예시 크기, 실제 파일 크기로 변경 필요
    bin_to_header(vocab_file, vocab_header_file, "vocab", vocab_size);

    return 0;
}
