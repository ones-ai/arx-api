#include <stdio.h>
#include <stdlib.h>

#define VOCAB_SIZE 65  // 예시 VOCAB_SIZE
#define EMBEDDING_DIM 64 // 예시 EMBEDDING_DIM

void bin_to_header(const char *bin_file, const char *header_file) {
    FILE *bin_fp = fopen(bin_file, "rb");
    if (!bin_fp) {
        perror("Failed to open binary file");
        exit(EXIT_FAILURE);
    }

    FILE *header_fp = fopen(header_file, "w");
    if (!header_fp) {
        perror("Failed to open header file");
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }

    // 헤더 파일의 시작 부분에 include 가드와 배열 선언 작성
    fprintf(header_fp, "#ifndef EMBEDDING_H\n");
    fprintf(header_fp, "#define EMBEDDING_H\n\n");
    fprintf(header_fp, "#define VOCAB_SIZE %d\n", VOCAB_SIZE);
    fprintf(header_fp, "#define EMBEDDING_DIM %d\n", EMBEDDING_DIM);
    fprintf(header_fp, "float embedding_vector[VOCAB_SIZE][EMBEDDING_DIM] = {\n");

    // 이진 파일에서 데이터를 읽어서 배열에 저장
    float value;
    for (int i = 0; i < VOCAB_SIZE; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < EMBEDDING_DIM; ++j) {
            if (fread(&value, sizeof(float), 1, bin_fp) != 1) {
                perror("Error reading binary file");
                fclose(bin_fp);
                fclose(header_fp);
                exit(EXIT_FAILURE);
            }
            // 파일에 float 값을 쓰고 마지막 값이 아니면 쉼표를 붙임
            fprintf(header_fp, "%f", value);
            if (j < EMBEDDING_DIM - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < VOCAB_SIZE - 1) {
            fprintf(header_fp, ",\n");
        }
    }

    // 배열과 include 가드 마무리
    fprintf(header_fp, "\n};\n\n");
    fprintf(header_fp, "#endif // EMBEDDING_H\n");

    // 파일 닫기
    fclose(bin_fp);
    fclose(header_fp);
    printf("Successfully generated header file: %s\n", header_file);
}

int main() {
    const char *bin_file = "param/embedding_weight_0.bin";
    const char *header_file = "build/include/small_embedding.h";

    bin_to_header(bin_file, header_file);

    return 0;
}
