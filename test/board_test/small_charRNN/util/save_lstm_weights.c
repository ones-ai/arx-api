#include <stdio.h>
#include <stdlib.h>

#define EMBEDDING_DIM 64    // 예시 값, 실제 값에 맞게 수정 필요
#define RNN_UNITS 64         // 예시 값, 실제 값에 맞게 수정 필요

// 배열 크기 정의
#define KERNEL_SIZE (EMBEDDING_DIM * 4 * RNN_UNITS)
#define RECURRENT_KERNEL_SIZE (RNN_UNITS * 4 * RNN_UNITS)
#define BIAS_SIZE (4 * RNN_UNITS)

// Function to extract weights from kernel and bias arrays and save to a header file
void extract_and_save_weights(const float *kernel, const float *recurrent_kernel, const float *bias, const char *header_file) {
    FILE *header_fp = fopen(header_file, "w");
    if (!header_fp) {
        perror("Failed to open header file");
        exit(EXIT_FAILURE);
    }

    // Declare weight and bias arrays
    float W_i[EMBEDDING_DIM][RNN_UNITS];
    float W_f[EMBEDDING_DIM][RNN_UNITS];
    float W_C[EMBEDDING_DIM][RNN_UNITS];
    float W_o[EMBEDDING_DIM][RNN_UNITS];
    
    float U_i[RNN_UNITS][RNN_UNITS];
    float U_f[RNN_UNITS][RNN_UNITS];
    float U_C[RNN_UNITS][RNN_UNITS];
    float U_o[RNN_UNITS][RNN_UNITS];

    float b_i[RNN_UNITS];
    float b_f[RNN_UNITS];
    float b_C[RNN_UNITS];
    float b_o[RNN_UNITS];

    // Extract weights from kernel array
    for (int i = 0; i < EMBEDDING_DIM; ++i) {
        for (int j = 0; j < RNN_UNITS; ++j) {
            W_i[i][j] = kernel[i * 4 * RNN_UNITS + j];
            W_f[i][j] = kernel[i * 4 * RNN_UNITS + RNN_UNITS + j];
            W_C[i][j] = kernel[i * 4 * RNN_UNITS + 2 * RNN_UNITS + j];
            W_o[i][j] = kernel[i * 4 * RNN_UNITS + 3 * RNN_UNITS + j];
        }
    }

    // Extract weights from recurrent_kernel array
    for (int i = 0; i < RNN_UNITS; ++i) {
        for (int j = 0; j < RNN_UNITS; ++j) {
            U_i[i][j] = recurrent_kernel[i * 4 * RNN_UNITS + j];
            U_f[i][j] = recurrent_kernel[i * 4 * RNN_UNITS + RNN_UNITS + j];
            U_C[i][j] = recurrent_kernel[i * 4 * RNN_UNITS + 2 * RNN_UNITS + j];
            U_o[i][j] = recurrent_kernel[i * 4 * RNN_UNITS + 3 * RNN_UNITS + j];
        }
    }

    // Extract biases from bias array
    for (int i = 0; i < RNN_UNITS; ++i) {
        b_i[i] = bias[i];
        b_f[i] = bias[RNN_UNITS + i];
        b_C[i] = bias[2 * RNN_UNITS + i];
        b_o[i] = bias[3 * RNN_UNITS + i];
    }

    // Write header file
    fprintf(header_fp, "#ifndef LSTM_WEIGHTS_H\n");
    fprintf(header_fp, "#define LSTM_WEIGHTS_H\n\n");

    fprintf(header_fp, "#define EMBEDDING_DIM %d\n", EMBEDDING_DIM);
    fprintf(header_fp, "#define RNN_UNITS %d\n", RNN_UNITS);

    fprintf(header_fp, "float W_i[EMBEDDING_DIM][RNN_UNITS] = {\n");
    for (int i = 0; i < EMBEDDING_DIM; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", W_i[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < EMBEDDING_DIM - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float W_f[EMBEDDING_DIM][RNN_UNITS] = {\n");
    for (int i = 0; i < EMBEDDING_DIM; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", W_f[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < EMBEDDING_DIM - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float W_C[EMBEDDING_DIM][RNN_UNITS] = {\n");
    for (int i = 0; i < EMBEDDING_DIM; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", W_C[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < EMBEDDING_DIM - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float W_o[EMBEDDING_DIM][RNN_UNITS] = {\n");
    for (int i = 0; i < EMBEDDING_DIM; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", W_o[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < EMBEDDING_DIM - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float U_i[RNN_UNITS][RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", U_i[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float U_f[RNN_UNITS][RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", U_f[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float U_C[RNN_UNITS][RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", U_C[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float U_o[RNN_UNITS][RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    {");
        for (int j = 0; j < RNN_UNITS; ++j) {
            fprintf(header_fp, "%.6f", U_o[i][j]);
            if (j < RNN_UNITS - 1) {
                fprintf(header_fp, ", ");
            }
        }
        fprintf(header_fp, "}");
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float b_i[RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    %.6f", b_i[i]);
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float b_f[RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    %.6f", b_f[i]);
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float b_C[RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    %.6f", b_C[i]);
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "float b_o[RNN_UNITS] = {\n");
    for (int i = 0; i < RNN_UNITS; ++i) {
        fprintf(header_fp, "    %.6f", b_o[i]);
        if (i < RNN_UNITS - 1) {
            fprintf(header_fp, ",\n");
        } else {
            fprintf(header_fp, "\n");
        }
    }
    fprintf(header_fp, "};\n\n");

    fprintf(header_fp, "#endif // LSTM_WEIGHTS_H\n");

    // Close file
    fclose(header_fp);
    printf("Successfully generated header file: %s\n", header_file);
}

int main() {
    const char *kernel_file = "param/lstm_weight_0.bin";
    const char *recurrent_kernel_file = "param/lstm_weight_1.bin";
    const char *bias_file = "param/lstm_weight_2.bin";
    const char *header_file = "build/include/small_lstm_weights.h";

    // Read kernel, recurrent_kernel, and bias data from binary files
    FILE *bin_fp = fopen(kernel_file, "rb");
    if (!bin_fp) {
        perror("Failed to open binary file");
        exit(EXIT_FAILURE);
    }

    float *kernel = (float *)malloc(KERNEL_SIZE * sizeof(float));
    if (!kernel) {
        perror("Failed to allocate memory");
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }

    if (fread(kernel, sizeof(float), KERNEL_SIZE, bin_fp) != KERNEL_SIZE) {
        perror("Error reading binary file");
        free(kernel);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }
    fclose(bin_fp);

    bin_fp = fopen(recurrent_kernel_file, "rb");
    if (!bin_fp) {
        perror("Failed to open binary file");
        free(kernel);
        exit(EXIT_FAILURE);
    }

    float *recurrent_kernel = (float *)malloc(RECURRENT_KERNEL_SIZE * sizeof(float));
    if (!recurrent_kernel) {
        perror("Failed to allocate memory");
        free(kernel);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }

    if (fread(recurrent_kernel, sizeof(float), RECURRENT_KERNEL_SIZE, bin_fp) != RECURRENT_KERNEL_SIZE) {
        perror("Error reading binary file");
        free(kernel);
        free(recurrent_kernel);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }
    fclose(bin_fp);

    bin_fp = fopen(bias_file, "rb");
    if (!bin_fp) {
        perror("Failed to open binary file");
        free(kernel);
        free(recurrent_kernel);
        exit(EXIT_FAILURE);
    }

    float *bias = (float *)malloc(BIAS_SIZE * sizeof(float));
    if (!bias) {
        perror("Failed to allocate memory");
        free(kernel);
        free(recurrent_kernel);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }

    if (fread(bias, sizeof(float), BIAS_SIZE, bin_fp) != BIAS_SIZE) {
        perror("Error reading binary file");
        free(kernel);
        free(recurrent_kernel);
        free(bias);
        fclose(bin_fp);
        exit(EXIT_FAILURE);
    }
    fclose(bin_fp);

    // Extract weights and save to header file
    extract_and_save_weights(kernel, recurrent_kernel, bias, header_file);

    // Free allocated memory
    free(kernel);
    free(recurrent_kernel);
    free(bias);

    return 0;
}
