#filepath = "/home/arx/rvx_release/platform/tip_arty/workspace/minicom_output.txt"
filepath = "./minicom_output.txt"

with open(filepath, 'r', encoding='utf-8') as file:
    content = file.read()

start_marker = "[EMU@FPGA]"
end_marker = "[section]"

start_index = content.find(start_marker)
end_index = content.find(end_marker)
if end_index == -1:
    end_index = len(content)

if start_index != -1:
    start_index += len(start_marker)
    while end_index > start_index and content[start_index].isspace():
        start_index += 1

if end_index != -1:
    while end_index > start_index and content[end_index - 1].isspace():
        end_index -= 1

if start_index != -1 and end_index != -1:
    extracted_text = content[start_index:end_index]

    with open(filepath, 'w', encoding='utf-8') as file:
        file.write(extracted_text)